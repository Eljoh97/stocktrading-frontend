///////////////////////////////////////////////
//              CONSTANTS            
//////////////////////////////////////////////


///////////////////////////////////////////////
//              Startup Task           
//////////////////////////////////////////////
$(".priceShowing").hide();
$("#canvas-div").hide();
$("#canvas-div2").hide();
$("#search-error-label").hide();
$("#state-label").hide();
$("#loader").hide();
UpdateTables();
$(".adviceShowing").hide();
$(".bollingbandShowing").hide();

// enter key to search for stock price and stats
document.getElementById("ticker")
    .addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            searchStock();
        }
    });

///////////////////////////////////////////////
//              Search Section         
//////////////////////////////////////////////    
function searchStock() {
    getPrice().then((price) => {
        price = price.toFixed(2);
        $("#price-label").text("$" + price)
        displayHistory();
        $(".priceShowing").show();
    }).catch(e => {
        $("#search-error-label").show();
        $(".priceShowing").hide();
    })
    $("#search-error-label").hide();
    $("#state-label").hide();
    $("#loader").hide();
    $(".adviceShowing").hide();
    $(".bollingbandShowing").hide();

}

///////////////////////////////////////////////
//        Price Display Section         
////////////////////////////////////////////// 
async function displayHistory() {

    document.getElementById('canvas').innerHTML = '<canvas id="canvas" width="1600" height="500"></canvas>';
    var currentDate = moment();
    var lastMonthDate = moment();
    lastMonthDate.subtract(1, 'months');

    var tradingDays = []
    for (var day = moment(lastMonthDate); day.diff(currentDate, 'days') <= 0; day.add(1, 'days')) {
        if (day.weekday() % 6) {
            tradingDays.push(moment(day).format('YYYY-MM-DD'));
        }
    }

    let response = await fetch(PRICE_API_URL + $(".ticker").val() + '/' + 'Adj Close' + '/' + lastMonthDate.format('YYYY-MM-DD') + '/' + currentDate.format('YYYY-MM-DD'));
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        $("#canvas-div").show();
        var priceHistory = await response.json();
        priceHistory = priceHistory.map(a => a.toFixed(2));

        if (priceHistory[priceHistory.length - 2] > priceHistory[priceHistory.length - 1]) {
            document.getElementById("price-label").style.color = 'red';
        } else {
            document.getElementById("price-label").style.color = 'green';
        }

        while (tradingDays.length > priceHistory.length) {
            tradingDays.shift();
        }

        var ctx = document.getElementById('canvas').getContext('2d');
        if (window.linechart && window.linechart !== null) {
            window.linechart.destroy();
        }
        window.linechart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: tradingDays,
                datasets: [{
                    data: priceHistory,
                    label: "Stock Price",
                    borderColor: "#3e95cd",
                    fill: false,
                    lineTension: 0
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Closing Prices for ' + $(".ticker").val() + ' for the Last 30 Days'
                },
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Data (Year/Month/Day)'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Price(USD)'
                        }
                    }]
                }
            }
        });
    }
}

async function TradeStock(side) {
    $("#buy-button").prop('disabled', true);
    $("#sell-button").prop('disabled', true);
    $("#state-label").show();
    $("#state-label").css('color', 'black');
    $("#state-label").html("Processing ");
    $("#loader").show();
    var temp = $("#price-label").text();
    var price = parseFloat(temp.split("$")[1])
    let data = {
        "ticker": $(".ticker").val(),
        "price": price,
        "state": "CREATED",
        "quantity": $(".quantity").val(),
        "side": side
    }
    addTrade(data).then((id) => {
        return new Promise(resolve => setTimeout(() => resolve(id), 8000));
    }).then((id) => getOneTrade(id)).then((trade) => {
        if (trade.state === "FILLED") {
            $("#state-label").show();
            $("#state-label").css('color', 'green');
            $("#state-label").text("Trade is Accepted");
            $("#loader").hide();
        } else {
            $("#state-label").show();
            $("#state-label").css('color', 'red');
            $("#state-label").text("Trade is Rejected");
            $("#loader").hide();
        }
        UpdateTables();
        $("#buy-button").prop('disabled', false);
        $("#sell-button").prop('disabled', false);
    });
}

///////////////////////////////////////////////
//              Advice Section            
//////////////////////////////////////////////
async function displayAdvice() {

    let response = await fetch(PRICE_API_URL + 'advice' + '/' + $(".ticker").val());
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

function getAdvice() {
    displayAdvice().then((advice) => {
        $("#advice-label").text(advice);
        $(".adviceShowing").show();
    }).catch(e => {
        $(".adviceShowing").hide();
    })

}

function getMoreInformation() {
    displayUpperband().then((ub) => {
        $("#upperband-label").text("$" + ub);
    }).catch(e => {

    })
    displayLowerband().then((lb) => {
        $("#lowerband-label").text("$" + lb)
    }).catch(e => {

    })
    displayMoreInformation();
    $(".bollingbandShowing").show();

}

async function displayMoreInformation() {
    $('#canvas1').remove();
    $('#canvas-div1').append('<canvas id="canvas1" width="1600" height="500"></canvas>');
    document.getElementById('canvas1').innerHTML = '<canvas id="canvas1" width="1600" height="500"></canvas>';
    var currentDate = moment();
    var lastMonthDate = moment();
    lastMonthDate.subtract(40, 'days');

    var tradingDays = []
    for (var day = moment(lastMonthDate); day.diff(currentDate, 'days') <= 0; day.add(1, 'days')) {
        if (day.weekday() % 6) {
            tradingDays.push(moment(day).format('YYYY-MM-DD'));
        }
    }

    let response = await fetch(PRICE_API_URL + 'moreinformation' + '/' + $(".ticker").val());
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {

        var priceHistory = await response.json();
        priceHistory[0] = priceHistory[0].map(a => a.toFixed(2));
        priceHistory[1] = priceHistory[1].map(a => a.toFixed(2));
        priceHistory[2] = priceHistory[2].map(a => a.toFixed(2));
        priceHistory[3] = priceHistory[3].map(a => a.toFixed(2));
        while (tradingDays.length > priceHistory[1].length) {
            tradingDays.shift();
        }


        var ctx = document.getElementById('canvas1').getContext('2d');

        var fillBetweenLinesPlugin = {
            afterDatasetsDraw: function(chart) {
                    var ctx = chart.chart.ctx;
                    var xaxis = chart.scales['x-axis-0'];
                    var yaxis = chart.scales['y-axis-0'];
                    var datasets = chart.data.datasets;
                    ctx.save();

                    for (var d = 0; d < datasets.length; d++) {
                        var dataset = datasets[d];
                        if (dataset.fillBetweenSet == undefined) {
                            continue;
                        }

                        // get meta for both data sets
                        var meta1 = chart.getDatasetMeta(d);
                        var meta2 = chart.getDatasetMeta(dataset.fillBetweenSet);

                        // do not draw fill if one of the datasets is hidden
                        if (meta1.hidden || meta2.hidden) continue;

                        // create fill areas in pairs
                        for (var p = 0; p < meta1.data.length - 1; p++) {
                            // if null skip
                            if (dataset.data[p] == null || dataset.data[p + 1] == null) continue;

                            ctx.beginPath();

                            // trace line 1
                            var curr = meta1.data[p];
                            var next = meta1.data[p + 1];
                            ctx.moveTo(curr._view.x, curr._view.y);
                            ctx.lineTo(curr._view.x, curr._view.y);
                            if (curr._view.steppedLine === true) {
                                ctx.lineTo(next._view.x, curr._view.y);
                                ctx.lineTo(next._view.x, next._view.y);
                            } else if (next._view.tension === 0) {
                                ctx.lineTo(next._view.x, next._view.y);
                            } else {
                                ctx.bezierCurveTo(
                                    curr._view.controlPointNextX,
                                    curr._view.controlPointNextY,
                                    next._view.controlPointPreviousX,
                                    next._view.controlPointPreviousY,
                                    next._view.x,
                                    next._view.y
                                );
                            }

                            // connect dataset1 to dataset2
                            var curr = meta2.data[p + 1];
                            var next = meta2.data[p];
                            ctx.lineTo(curr._view.x, curr._view.y);

                            // trace BACKWORDS set2 to complete the box
                            if (curr._view.steppedLine === true) {
                                ctx.lineTo(curr._view.x, next._view.y);
                                ctx.lineTo(next._view.x, next._view.y);
                            } else if (next._view.tension === 0) {
                                ctx.lineTo(next._view.x, next._view.y);
                            } else {
                                // reverse bezier
                                ctx.bezierCurveTo(
                                    curr._view.controlPointPreviousX,
                                    curr._view.controlPointPreviousY,
                                    next._view.controlPointNextX,
                                    next._view.controlPointNextY,
                                    next._view.x,
                                    next._view.y
                                );
                            }

                            // close the loop and fill with shading
                            ctx.closePath();
                            ctx.fillStyle = dataset.fillBetweenColor || "rgba(0,0,0,0.1)";
                            ctx.fill();
                        } // end for p loop
                    }
                } // end afterDatasetsDraw
        }; // end fillBetweenLinesPlugin

        Chart.pluginService.register(fillBetweenLinesPlugin);

        var AdviceChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: tradingDays,
                datasets: [{
                        data: priceHistory[0],
                        label: "Close",
                        borderColor: "#ADD8E6",
                        fill: false,
                        lineTension: 0
                    },
                    {
                        data: priceHistory[1],
                        label: "Upper Band",
                        borderColor: "#FFFF00",
                        fill: false,
                        lineTension: 0,
                        fillBetweenSet: 1,
                        fillBetweenColor: "rgba(128,128,128, 0.2)"
                    },
                    {
                        data: priceHistory[2],
                        label: "Lower Band",
                        borderColor: "#800080",
                        fill: false,
                        lineTension: 0,
                        fillBetweenSet: 1,
                        fillBetweenColor: "rgba(128,128,128, 0.2)"
                    },
                    {
                        data: priceHistory[3],
                        label: "Mavg",
                        borderColor: "#800000",
                        fill: false,
                        lineTension: 0
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: '21 Day Bollinger Band for ' + $(".ticker").val()
                },
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Data (Year/Month/Day)'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Price(USD)'
                        }
                    }]
                }
            }
        });
    }
}

///////////////////////////////////////////////
//              Table Section            
//////////////////////////////////////////////

async function UpdateTables() {
    let tradeData = await getAllTrades();
    let holdingData = await getAllHoldings();
    holdingData = await addPriceToHolding(holdingData);
    tradehtmlString = createTradeTable(tradeData);
    holdinghtmlString = createHoldingTable(holdingData);
    document.getElementById("tradeDiv").innerHTML = tradehtmlString;
    document.getElementById("holdingDiv").innerHTML = holdinghtmlString;

    tickers = [];
    quantities = [];
    colours = [];

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };

    for (var i = 0; i < holdingData.length; i++) {
        tickers.push(holdingData[i].ticker);
        quantities.push(holdingData[i].quantity);
        colours.push(dynamicColors());
    }

    var ctx = document.getElementById('canvas2').getContext('2d');
    if (window.doughnut && window.doughnut !== null) {
        window.doughnut.destroy();
    }
    window.doughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: tickers,
            datasets: [{
                data: quantities,
                backgroundColor: colours,
                fill: true,
            }]
        },
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70
        }
    });
}

function createHoldingTable(holdingData) {
    let htmlString = "<table class='table table-striped'><thead><tr><th>Date</th><th>Ticker</th>";
    htmlString += "<th>Quantity</th><th>Current Price ($)</th><th>Total Value ($)</th>";
    htmlString += "</tr></thead>";
    $("#canvas-div2").show();

    tickers = [];
    quantities = [];
    colours = [];

    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };

    for (var i = 0; i < holdingData.length; i++) {
        tickers.push(holdingData[i].ticker);
        quantities.push(holdingData[i].quantity);
        colours.push(dynamicColors());
    }

    var ctx = document.getElementById('canvas2').getContext('2d');
    if (window.doughnut && window.doughnut !== null) {
        window.doughnut.destroy();
    }
    window.doughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: tickers,
            datasets: [{
                data: quantities,
                backgroundColor: colours,
                fill: true,
            }]
        },
        options: {
            legend: {
                display: false
            },
            cutoutPercentage: 70
        }
    });

    holdingData.reverse();
    let totalNetWorth = 0;
    holdingData.map((holding) => {
        totalNetWorth += holding.currentPrice * holding.quantity;
        htmlString += "<tr>";
        htmlString += "<td>" + holding.id.date.substring(0, 10) + "</td>"; //new
        htmlString += "<td>" + holding.ticker + "</td>";
        htmlString += "<td>" + holding.quantity + "</td>";
        htmlString += "<td>" + holding.currentPrice + "</td>";
        htmlString += "<td>" + (holding.currentPrice * holding.quantity).toFixed(2) + "</td>";
        htmlString += "</tr>";
    });
    $("#NetWorth").text("Portfolio Value: $" + totalNetWorth.toFixed(2));
    htmlString += "</table>";

    return htmlString;
}

function createTradeTable(tradeData) {
    let htmlString = "<table class='table table-striped'><thead><tr><th>Date</th><th>Ticker</th>";
    htmlString += "<th>Quantity</th><th>Price ($)</th><th>Type</th><th>State</th>";
    htmlString += "</tr></thead>";

    tradeData.reverse();
    tradeData.map((trade) => {
        htmlString += "<tr>";
        htmlString += "<td>" + trade.id.date.substring(0, 10) + "</td>"; //new
        htmlString += "<td>" + trade.ticker + "</td>";
        htmlString += "<td>" + trade.quantity + "</td>";
        htmlString += "<td>" + trade.price.toFixed(2) + "</td>";
        htmlString += "<td>" + trade.side + "</td>";
        htmlString += "<td>" + trade.state + "</td>";
        htmlString += "</tr>";
    });
    htmlString += "</table>";

    return htmlString;
}

///////////////////////////////////////////////
//              API fetches            
//////////////////////////////////////////////

async function addTrade(data) {
    let response = await fetch(STOCKTRADING_API_URL + '/stock', {
        method: 'POST',
        headers: {
            'charset': 'UTF-8',
            'Accept': '*/*',
            'Content-Type': 'application/json;charset=UTF-8',
            "Access-Control-Origin": "*"
        },
        body: JSON.stringify(data)
    });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.text();
    }
}

async function getOneTrade(id) {
    let response = await fetch(STOCKTRADING_API_URL + '/stock/' + id, {
        method: 'GET'
    });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

async function getAllTrades() {
    let response = await fetch(STOCKTRADING_API_URL + '/stock');
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

async function getAllHoldings() {
    let response = await fetch(STOCKTRADING_API_URL + '/stock/holding');
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

async function getPrice(ticker = $(".ticker").val()) {
    let response = await fetch(PRICE_API_URL + ticker + '/');
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

async function addPriceToHolding(holdingData) {
    for (var i = 0; i < holdingData.length; i++) {
        let currentPrice = await getPrice(holdingData[i].ticker).then((price) => {
            price = price.toFixed(2);
            return price;
        });
        holdingData[i]["currentPrice"] = currentPrice;
    }
    return await holdingData;
}

async function displayUpperband() {
    let response = await fetch(PRICE_API_URL + 'advice' + '/' + 'upperband' + '/' + $(".ticker").val());
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}

async function displayLowerband() {
    let response = await fetch(PRICE_API_URL + 'advice' + '/' + 'lowerband' + '/' + $(".ticker").val());
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json();
    }
}