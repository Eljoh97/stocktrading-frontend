FROM nginx:alpine
COPY . /usr/share/nginx/html
RUN sh -c "echo PRICE_API_URL=\'http://pricing.abdullahsumbal.com/\' > /usr/share/nginx/html/url.js"
RUN sh -c "echo STOCKTRADING_API_URL = \'http://trading.abdullahsumbal.com/\' >> /usr/share/nginx/html/url.js"
